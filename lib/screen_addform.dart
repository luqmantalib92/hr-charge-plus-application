import 'dart:convert';

import 'package:flutter/material.dart';

import 'data/data_cache.dart';
import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';
import 'style/style_decoration.dart';
import 'style/style_font.dart';
import 'style/style_input.dart';

import 'widget/widget_addchecklist.dart';
import 'widget/widget_applicationbar.dart';
import 'widget/widget_buttonbottom.dart';
import 'widget/widget_cardchecklist.dart';
import 'widget/widget_dialog.dart';

class ScreenAddForm extends StatefulWidget {
  @override
  _ScreenAddFormState createState() => _ScreenAddFormState();
}

class _ScreenAddFormState extends State<ScreenAddForm> {
  // VARIABLE
  // Input controller properties
  TextEditingController _controllerTitle = TextEditingController();
  // Checklist properties
  List<CardChecklist> _listChecklist = [];
  int _totalChecklist = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Cache.checklist = [];
  }

  // CREATE NEW CHECKLIST
  void createChecklist() {
    print("Create checklist");
    // Initialize local properties
    List<CardChecklist> listChecklist = _listChecklist;
    int totalChecklist = _totalChecklist;

    // Initialize checklist data
    Cache.checklist.add(
      {
        "task": "Checklist ${totalChecklist + 1}",
        "duration": "1",
        "status": "false"
      },
    );

    // Create new checklist
    listChecklist.add(
      CardChecklist(
        title: Cache.checklist[totalChecklist]["task"].toString(),
        onPressed: () {
          print("Update checklist $totalChecklist");
          print(Cache.checklist);
          WidgetDialog().checklist(
            context: context,
            id: totalChecklist,
            controllerTitle: TextEditingController(
              text: Cache.checklist[totalChecklist]["task"].toString(),
            ),
            controllerDuration: TextEditingController(
              text: Cache.checklist[totalChecklist]["duration"].toString(),
            ),
          );
        },
      ),
    );

    setState(() {
      _listChecklist = listChecklist;
      _totalChecklist++;
    });
  }

  // ADD NEW FORM TO DATABASE
  Future addNewForm() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    // Validate form
    if (_controllerTitle.text == "" || _controllerTitle.text == null) {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please fill role title',
        titleButton: 'Proceed',
      );
      return;
    }
    if (Cache.checklist.length <= 0) {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please create at least 1 checklist',
        titleButton: 'Proceed',
      );
      return;
    }

    // Get form last id
    Map rawdata = await Dummy().getData(file.form);
    List data = rawdata["data"];
    int dataLength = data.length;
    String lastId = data[dataLength - 1]["id"].toString();
    int newId = int.parse(lastId) + 1;

    // Create user profile data
    String id = newId.toString();
    String title = _controllerTitle.text;

    // Save to static dummy data
    rawdata = await Dummy().getData(file.form);
    List formList = rawdata["data"];
    formList.add({
      "id": id,
      "title": title,
      "data": Cache.checklist,
    });
    Map formData = {"data": formList};
    String jsonString = json.encode(formData);
    Dummy().setData(file.form, jsonString);

    WidgetDialog().acknowledged(
      context: context,
      title: "Success",
      description: "New form successfuly created!",
      titleButton: "Proceed",
      onPressedButton: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      backgroundColor: Colour.bgPrimary,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: [
            ApplicationBar(
              title: "Add New Form",
              enableBack: true,
            ),
            Container(
              height: double.infinity,
              width: double.infinity,
              margin: EdgeInsets.only(top: Value.val100),
              padding: EdgeInsets.only(bottom: Value.val50),
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                color: Colour.bgScaffold,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Value.val40),
                  topRight: Radius.circular(Value.val40),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: Value.val30),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Form Title",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Container(
                      decoration: StyleDecoration.basic(),
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: TextFormField(
                        controller: _controllerTitle,
                        style: StyleFont.inter16px400w(
                          Colour.textPrimary,
                        ),
                        keyboardType: TextInputType.text,
                        decoration: StyleInput().border(
                          hintText: '',
                          hintStyle: StyleFont.inter14px400w(
                            Colour.textSecondary,
                          ),
                          fillColor: Colour.white,
                          borderColor: Colour.transparent,
                          borderThick: 0,
                          radius: 10,
                        ),
                      ),
                    ),
                    SizedBox(height: Value.val10),
                    Column(
                      children: _listChecklist,
                    ),
                    AddChecklist(
                      totalChecklist: _totalChecklist,
                      onPressed: createChecklist,
                    ),
                  ],
                ),
              ),
            ),
            ButtonBottom(
              text: "Add New Form",
              onPressed: addNewForm,
            ),
          ],
        ),
      ),
    );
  }
}
