import 'package:flutter/material.dart';

import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';

import 'widget/widget_applicationbar.dart';
import 'widget/widget_navigationbar.dart';
import 'widget/widget_buttonfloating.dart';
import 'widget/widget_cardrole.dart';

class ScreenHRRole extends StatefulWidget {
  @override
  _ScreenHRRoleState createState() => _ScreenHRRoleState();
}

class _ScreenHRRoleState extends State<ScreenHRRole> {
  // VARIABLE
  // List roles properties
  List<CardRole> _listRoles = [];

  // INTI STATE
  @override
  void initState() {
    super.initState();
    getListRoles();
  }

  // GET LIST ROLES
  Future getListRoles() async {
    List<CardRole> listRoles = [];
    Map rawdata = await Dummy().getData(file.role);
    List data = rawdata["data"];
    int dataLength = data.length;

    // loop all data
    for (int i = 0; i < dataLength; i++) {
      String roleTitle = data[i]["title"].toString();
      String roleForm = data[i]["form_name"].toString();
      listRoles.add(
        CardRole(
          role: roleTitle,
          form: roleForm,
        ),
      );
    }
    // Set global
    setState(() {
      _listRoles = listRoles;
    });
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      floatingActionButton: ButtonFloating(
        tooltip: "Add new role",
        navigate: pages.addNewRole,
      ),
      backgroundColor: Colour.bgPrimary,
      body: Stack(
        children: [
          ApplicationBar(title: "Role"),
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(top: Value.val100),
            padding: EdgeInsets.only(bottom: Value.val50),
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              color: Colour.bgScaffold,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(Value.val40),
                topRight: Radius.circular(Value.val40),
              ),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: Value.val30),
                  Container(
                    child: Column(children: _listRoles),
                  ),
                  SizedBox(height: Value.val40),
                ],
              ),
            ),
          ),

          // Display navigation bar
          NavigationBar(index: 1, role: roles.hr),
        ],
      ),
    );
  }
}
