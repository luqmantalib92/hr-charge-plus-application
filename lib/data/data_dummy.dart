import 'dart:convert';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:path_provider/path_provider.dart';

enum file { form, role, staff }

class Dummy {
  Future getData(file files) async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path;

    if (files == file.form) {
      File file = File('$path/forms.json');
      String jsonString = await file.readAsString();
      Map rawData = jsonDecode(jsonString);
      return rawData;
    } else if (files == file.role) {
      File file = File('$path/roles.json');
      String jsonString = await file.readAsString();
      Map rawData = jsonDecode(jsonString);
      return rawData;
    } else if (files == file.staff) {
      File file = File('$path/staffs.json');
      String jsonString = await file.readAsString();
      Map rawData = json.decode(jsonString);
      return rawData;
    }
  }

  Future setData(file files, String jsonString) async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path;

    if (files == file.form) {
      File file = File('$path/forms.json');
      file.writeAsString(jsonString);
    } else if (files == file.role) {
      File file = File('$path/roles.json');
      file.writeAsString(jsonString);
    } else if (files == file.staff) {
      File file = File('$path/staffs.json');
      file.writeAsString(jsonString);
    }
  }

  Future createFileApplication(context) async {
    Directory directory = await getApplicationDocumentsDirectory();
    String path = directory.path;

    // Write forms data
    File file = File('$path/forms.json');
    bool exists = await file.exists();
    if (!exists) {
      print("File forms.json not exist!");
      String rawFile = await DefaultAssetBundle.of(context).loadString(
        "assets/json/forms.json",
      );
      file.writeAsString(rawFile);
    } else {
      print("File forms.json already exist!");
    }

    // Write roles data
    file = File('$path/roles.json');
    exists = await file.exists();
    if (!exists) {
      print("File roles.json not exist!");
      String rawFile = await DefaultAssetBundle.of(context).loadString(
        "assets/json/roles.json",
      );
      file.writeAsString(rawFile);
    } else {
      print("File roles.json already exist!");
    }

    // Write staffs data
    file = File('$path/staffs.json');
    exists = await file.exists();
    if (!exists) {
      print("File staffs.json not exist!");
      String rawFile = await DefaultAssetBundle.of(context).loadString(
        "assets/json/staffs.json",
      );
      file.writeAsString(rawFile);
    } else {
      print("File staffs.json already exist!");
    }
  }
}
