import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'screen_editstaffform.dart';
import 'style/style_appbar.dart';
import 'style/style_color.dart';
import 'style/style_font.dart';

import 'widget/widget_applicationbar.dart';
import 'widget/widget_navigationbar.dart';
import 'widget/widget_dropdown.dart';
import 'widget/widget_buttonfloating.dart';
import 'widget/widget_cardstaff.dart';
import 'widget/widget_containerbody.dart';

class ScreenHRStaff extends StatefulWidget {
  @override
  _ScreenHRStaffState createState() => _ScreenHRStaffState();
}

class _ScreenHRStaffState extends State<ScreenHRStaff> {
  // VARIABLE
  // Dropdown for role selection properties
  List<DropdownMenuItem<dynamic>> _dropdownRole = [
    DropdownMenuItem(child: Text("All Staff"), value: "0"),
  ];
  String _selectedRole = "0";
  // List staffs properties
  List<CardStaff> _listStaffs = [];

  // INTI STATE
  @override
  void initState() {
    super.initState();
    getListRoles();
    getListStaffs("0");
  }

  // GET LIST ROLES FOR DROPDOWN BUTTON
  Future getListRoles() async {
    Map rawdata = await Dummy().getData(file.role);
    List data = rawdata["data"];
    int dataLength = data.length;
    // loop all data
    for (int i = 0; i < dataLength; i++) {
      String roleId = data[i]["id"].toString();
      String roleTitle = data[i]["title"].toString();
      _dropdownRole.add(
        DropdownMenuItem(child: Text(roleTitle), value: roleId),
      );
    }
  }

  // GET LIST STAFFS FOR LIST
  Future getListStaffs(String selectedRole) async {
    List<CardStaff> listStaffs = [];
    Map rawdata = await Dummy().getData(file.staff);
    List data = rawdata["data"];
    int dataLength = data.length;
    // loop all data
    for (int i = 0; i < dataLength; i++) {
      String staffId = data[i]["id"].toString();
      String staffName = data[i]["name"].toString();
      String staffRoleId = data[i]["role_id"].toString();
      String staffProfileURL = data[i]["profile_url"].toString();

      if (selectedRole == "0" || selectedRole == staffRoleId) {
        listStaffs.add(
          CardStaff(
            id: staffId,
            name: staffName,
            profileURL: staffProfileURL,
            onPressed: () async {
              await Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.fade,
                  child: ScreenEditStaffForm(
                    index: i,
                    staffData: data[i],
                  ),
                ),
              );
              // Reload whole page
              Navigator.pushReplacement(
                context,
                PageTransition(
                  type: PageTransitionType.fade,
                  child: ScreenHRStaff(),
                ),
              );
            },
          ),
        );
      }
    }
    // Set global
    setState(() {
      _listStaffs = listStaffs;
    });
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      floatingActionButton: ButtonFloating(
        tooltip: "Add new staff",
        navigate: pages.addNewStaff,
      ),
      backgroundColor: Colour.bgPrimary,
      body: Stack(
        children: [
          ApplicationBar(title: "Staff"),
          ContainerBody(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  Padding(
                    padding: EdgeInsets.symmetric(
                      vertical: Value.val20,
                      horizontal: Value.val30,
                    ),
                    child: DropDown(
                      dropdownItems: _dropdownRole,
                      selectedItem: _selectedRole,
                      onChanged: (selected) {
                        setState(() {
                          _selectedRole = selected;
                          getListStaffs(_selectedRole);
                        });
                      },
                    ),
                  ),
                  SizedBox(height: Value.val10),
                  Container(
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: Value.val30),
                    child: Text(
                      "List Staff",
                      style: StyleFont.inter24px700w(Colors.black),
                      textAlign: TextAlign.left,
                    ),
                  ),
                  SizedBox(height: Value.val10),
                  Container(
                    child: Column(
                      children: _listStaffs,
                    ),
                  ),
                  SizedBox(height: Value.val40),
                ],
              ),
            ),
          ),
          // Display navigation bar
          NavigationBar(index: 0, role: roles.hr),
        ],
      ),
    );
  }
}
