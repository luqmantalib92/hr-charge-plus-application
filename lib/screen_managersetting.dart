import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';
import 'style/style_font.dart';

import 'widget/widget_navigationbar.dart';
import 'widget/widget_cardsettings.dart';
import 'widget/widget_buttoninvisible.dart';

import 'screen_login.dart';

class ScreenManagerSetting extends StatefulWidget {
  @override
  _ScreenManagerSettingState createState() => _ScreenManagerSettingState();
}

class _ScreenManagerSettingState extends State<ScreenManagerSetting> {
  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      body: Stack(
        children: [
          Container(
            height: double.infinity,
            width: double.infinity,
            child: Column(
              children: [
                Expanded(
                  child: Container(
                    child: SingleChildScrollView(
                      scrollDirection: Axis.vertical,
                      child: Container(
                        child: Align(
                          alignment: Alignment.topCenter,
                          child: Container(
                            padding: EdgeInsets.symmetric(
                              horizontal: Value.val25,
                            ),
                            child: Column(
                              children: [
                                // Label title
                                SizedBox(height: Value.val20),
                                Container(
                                  alignment: Alignment.centerLeft,
                                  child: Text(
                                    "Settings",
                                    style: StyleFont.inter34px700w(
                                      Colour.textPrimary,
                                    ),
                                  ),
                                ),
                                // Button profile
                                SizedBox(height: Value.val35),
                                CardSettings(
                                  imagePath: "assets/card.png",
                                  imageSize: Value.val30,
                                  imageColor: Colour.textSecondary,
                                  title: "Profile",
                                  content: "Manage Your Profile",
                                  onPressed: () {},
                                ),
                                // Button settings
                                SizedBox(height: Value.val10),
                                CardSettings(
                                  imagePath: "assets/setting.png",
                                  imageSize: Value.val30,
                                  imageColor: Colour.textSecondary,
                                  title: "Settings",
                                  content: "View More Settings",
                                  onPressed: () {},
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ),

                // SECTION [Logout]
                Container(
                  child: ButtonInvisible(
                    padding: EdgeInsets.all(Value.val25),
                    onPressed: () {
                      Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ScreenLogin(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text(
                          "Sign Out",
                          style: StyleFont.inter14px700w(Colour.bgAlert),
                        ),
                        SizedBox(width: Value.val10),
                        Image.asset(
                          "assets/logout.png",
                          width: Value.val20,
                          color: Colour.bgAlert,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: Value.val90),
              ],
            ),
          ),

          // Display navigation bar
          NavigationBar(index: 1, role: roles.manager),
        ],
      ),
    );
  }
}
