import 'package:flutter/material.dart';

import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';

import 'widget/widget_applicationbar.dart';
import 'widget/widget_navigationbar.dart';
import 'widget/widget_cardform.dart';
import 'widget/widget_buttonfloating.dart';

class ScreenHRForm extends StatefulWidget {
  @override
  _ScreenHRFormState createState() => _ScreenHRFormState();
}

class _ScreenHRFormState extends State<ScreenHRForm> {
  // VARIABLE
  // List staffs properties
  List<CardForm> _listForms = [];

  // INTI STATE
  @override
  void initState() {
    super.initState();
    getListForms();
  }

  // GET LIST FORMS
  Future getListForms() async {
    List<CardForm> listForms = [];
    Map rawdata = await Dummy().getData(file.form);
    List data = rawdata["data"];
    int dataLength = data.length;

    // loop all data
    for (int i = 0; i < dataLength; i++) {
      String formId = data[i]["id"].toString();
      String formTitle = data[i]["title"].toString();
      List formData = data[i]["data"];
      listForms.add(
        CardForm(
          id: formId,
          title: formTitle,
          data: formData,
        ),
      );
    }
    // Set global
    setState(() {
      _listForms = listForms;
    });
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      floatingActionButton: ButtonFloating(
        tooltip: "Add new form",
        navigate: pages.addNewForm,
      ),
      backgroundColor: Colour.bgPrimary,
      body: Stack(
        children: [
          ApplicationBar(title: "Form"),
          Container(
            height: double.infinity,
            width: double.infinity,
            margin: EdgeInsets.only(top: Value.val100),
            padding: EdgeInsets.only(bottom: Value.val50),
            clipBehavior: Clip.hardEdge,
            decoration: BoxDecoration(
              color: Colour.bgScaffold,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(Value.val40),
                topRight: Radius.circular(Value.val40),
              ),
            ),
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: Value.val30),
                  Container(
                    child: Column(
                      children: _listForms,
                    ),
                  ),
                  SizedBox(height: Value.val40),
                ],
              ),
            ),
          ),

          // Display navigation bar
          NavigationBar(index: 2, role: roles.hr),
        ],
      ),
    );
  }
}
