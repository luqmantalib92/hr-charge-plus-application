import 'dart:convert';

import 'package:flutter/material.dart';

import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';
import 'style/style_decoration.dart';
import 'style/style_font.dart';
import 'style/style_input.dart';

import 'widget/widget_applicationbar.dart';
import 'widget/widget_buttonbottom.dart';
import 'widget/widget_dialog.dart';
import 'widget/widget_dropdown.dart';

class ScreenAddRole extends StatefulWidget {
  @override
  _ScreenAddRoleState createState() => _ScreenAddRoleState();
}

class _ScreenAddRoleState extends State<ScreenAddRole> {
  // VARIABLE
  // Input controller properties
  TextEditingController _controllerTitle = TextEditingController();
  // Dropdown for role selection properties
  List<DropdownMenuItem<dynamic>> _dropdownForm = [
    DropdownMenuItem(
      child: Text(
        "Attach form",
        style: StyleFont.inter16px400w(
          Colour.textSecondary,
        ),
      ),
      value: "0",
    ),
  ];
  String _selectedForm = "0";
  // Create role properties
  String _lastId = "";

  // INTI STATE
  @override
  void initState() {
    super.initState();
    getLastRoleId();
    getListForms();
  }

  // GET LAST ROLE ID
  Future getLastRoleId() async {
    Map rawdata = await Dummy().getData(file.role);
    List data = rawdata["data"];
    int dataLength = data.length;
    String lastId = data[dataLength - 1]["id"].toString();
    setState(() {
      _lastId = lastId;
    });
  }

  // GET LIST FORMS FOR DROPDOWN BUTTON
  Future getListForms() async {
    Map rawdata = await Dummy().getData(file.form);
    List data = rawdata["data"];
    int dataLength = data.length;
    // loop all data
    for (int i = 0; i < dataLength; i++) {
      String roleId = data[i]["id"].toString();
      String roleTitle = data[i]["title"].toString();
      _dropdownForm.add(
        DropdownMenuItem(child: Text(roleTitle), value: roleId),
      );
    }
  }

  // ADD NEW ROLE TO DATABASE
  Future addNewRole() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    // Validate form
    if (_controllerTitle.text == "" || _controllerTitle.text == null) {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please fill role title',
        titleButton: 'Proceed',
      );
      return;
    }
    if (_selectedForm == "0") {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please select a form',
        titleButton: 'Proceed',
      );
      return;
    }

    // Initialize form variable
    String formId = "0";
    String formName = "";
    int newId = int.parse(_lastId);
    newId += 1;

    // Find form data with form id
    Map rawdata = await Dummy().getData(file.form);
    List data = rawdata["data"];
    int dataLength = data.length;
    for (int i = 0; i < dataLength; i++) {
      if (data[i]["id"] == _selectedForm) {
        formId = data[i]["id"];
        formName = data[i]["title"];
        // End loop
        i = dataLength;
      }
    }

    // Create user profile data
    String id = newId.toString();
    String title = _controllerTitle.text;

    // Save to static dummy data
    rawdata = await Dummy().getData(file.role);
    List formList = rawdata["data"];
    formList.add({
      "id": id,
      "title": title,
      "form_id": formId,
      "form_name": formName,
    });
    Map formData = {"data": formList};
    String jsonString = json.encode(formData);
    Dummy().setData(file.role, jsonString);

    WidgetDialog().acknowledged(
      context: context,
      title: "Success",
      description: "New role successfuly created!",
      titleButton: "Proceed",
      onPressedButton: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      backgroundColor: Colour.bgPrimary,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: [
            ApplicationBar(
              title: "Add New Role",
              enableBack: true,
            ),
            Container(
              height: double.infinity,
              width: double.infinity,
              margin: EdgeInsets.only(top: Value.val100),
              padding: EdgeInsets.only(bottom: Value.val50),
              clipBehavior: Clip.hardEdge,
              decoration: BoxDecoration(
                color: Colour.bgScaffold,
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(Value.val40),
                  topRight: Radius.circular(Value.val40),
                ),
              ),
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: Value.val30),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Role Title",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Container(
                      decoration: StyleDecoration.basic(),
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: TextFormField(
                        controller: _controllerTitle,
                        style: StyleFont.inter16px400w(
                          Colour.textPrimary,
                        ),
                        keyboardType: TextInputType.text,
                        decoration: StyleInput().border(
                          hintText: '',
                          hintStyle: StyleFont.inter14px400w(
                            Colour.textSecondary,
                          ),
                          fillColor: Colour.white,
                          borderColor: Colour.transparent,
                          borderThick: 0,
                          radius: 10,
                        ),
                      ),
                    ),
                    SizedBox(height: Value.val20),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Role",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: Value.val30,
                      ),
                      child: DropDown(
                        dropdownItems: _dropdownForm,
                        selectedItem: _selectedForm,
                        onChanged: (selected) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          setState(() {
                            _selectedForm = selected;
                          });
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ButtonBottom(
              text: "Add New Role",
              onPressed: addNewRole,
            ),
          ],
        ),
      ),
    );
  }
}
