import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'data/data_cache.dart';
import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';

import 'widget/widget_applicationbar.dart';
import 'widget/widget_navigationbar.dart';
import 'widget/widget_cardstaff.dart';
import 'widget/widget_containerbody.dart';

import 'screen_editstaffform.dart';

class ScreenManagerStaff extends StatefulWidget {
  @override
  _ScreenManagerStaffState createState() => _ScreenManagerStaffState();
}

class _ScreenManagerStaffState extends State<ScreenManagerStaff> {
  // VARIABLE
  // List staffs properties
  List<CardStaff> _listStaffs = [];

  // INTI STATE
  @override
  void initState() {
    super.initState();
    getListStaffs();
  }

  // GET LIST STAFFS FOR LIST
  Future getListStaffs() async {
    List<CardStaff> listStaffs = [];
    Map rawdata = await Dummy().getData(file.staff);
    List data = rawdata["data"];
    int dataLength = data.length;
    // loop all data
    for (int i = 0; i < dataLength; i++) {
      String staffId = data[i]["id"].toString();
      String staffName = data[i]["name"].toString();
      String staffProfileURL = data[i]["profile_url"].toString();
      List picIds = data[i]["pic_ids"];

      // Check if staff belongs to correct person in charge
      if (picIds.contains(Cache.id)) {
        listStaffs.add(
          CardStaff(
            id: staffId,
            name: staffName,
            profileURL: staffProfileURL,
            onPressed: () async {
              await Navigator.push(
                context,
                PageTransition(
                  type: PageTransitionType.fade,
                  child: ScreenEditStaffForm(
                    index: i,
                    staffData: data[i],
                    isHRView: false,
                  ),
                ),
              );
              // Reload whole page
              Navigator.pushReplacement(
                context,
                PageTransition(
                  type: PageTransitionType.fade,
                  child: ScreenManagerStaff(),
                ),
              );
            },
          ),
        );
      }
    }

    // Set global
    setState(() {
      _listStaffs = listStaffs;
    });
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      backgroundColor: Colour.bgPrimary,
      body: Stack(
        children: [
          ApplicationBar(title: "Staff"),
          ContainerBody(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: Value.val20),
                  Container(
                    child: Column(
                      children: _listStaffs,
                    ),
                  ),
                  SizedBox(height: Value.val40),
                ],
              ),
            ),
          ),

          // Display navigation bar
          NavigationBar(index: 0, role: roles.manager),
        ],
      ),
    );
  }
}
