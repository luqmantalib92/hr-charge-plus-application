import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';
import 'package:date_time_picker/date_time_picker.dart';

import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';
import 'style/style_font.dart';

import 'widget/widget_applicationbar.dart';
import 'widget/widget_buttonbottom.dart';
import 'widget/widget_dialog.dart';
import 'widget/widget_containerbody.dart';

class ScreenEditStaffForm extends StatefulWidget {
  final int index;
  final Map staffData;
  final bool isHRView;
  ScreenEditStaffForm({
    @required this.index,
    @required this.staffData,
    this.isHRView = true,
  });

  @override
  _ScreenEditStaffFormState createState() => _ScreenEditStaffFormState();
}

class _ScreenEditStaffFormState extends State<ScreenEditStaffForm> {
  // VARIABLE
  List<Widget> _checklist = [];
  List<bool> _checklistCheckbox = [];
  List _staffChecklistData = [];

  int _index = 0;
  Map _staffData = Map();

  // INTI STATE
  @override
  void initState() {
    super.initState();
    initFunction();
  }

  Future initFunction() async {
    await initVariable();
    await getStaffForm();
  }

  // INITIALIZE VARIABLES
  Future initVariable() async {
    // Sort staff form data
    List data = widget.staffData["data"];
    data.sort((a, b) => a["duration"].compareTo(b["duration"]));

    // Set global
    setState(() {
      _index = widget.index;
      _staffData = widget.staffData;
      _staffChecklistData = data;
    });
  }

  // GET STAFF FORM
  Future getStaffForm() async {
    List<Widget> checklist = [];
    setState(() {
      _checklistCheckbox = [];
    });

    // Convert to date format
    String dateString = _staffData["date_duty"].toString();
    DateTime dateDuty = DateTime.parse(dateString);

    // Load data
    List data = _staffChecklistData;
    int dataLength = data.length;
    String currentDuration = "";

    // Sort staff form data
    data.sort((a, b) => a["duration"].compareTo(b["duration"]));
    // Loop all data
    for (int i = 0; i < dataLength; i++) {
      int indx = i;
      // Set global
      setState(() {
        _checklistCheckbox.add(
          data[i]["status"].toString().toLowerCase() == "true" ? true : false,
        );
      });

      // Create date title on top
      if (currentDuration != data[i]["duration"]) {
        currentDuration = data[i]["duration"];
        int addDuration = int.parse(currentDuration);
        DateTime checklistDate = dateDuty.add(Duration(days: addDuration));
        String formattedDate = DateFormat('MMMM d, yyyy').format(checklistDate);
        checklist.add(
          Container(
            alignment: Alignment.centerLeft,
            margin: EdgeInsets.only(top: Value.val10),
            width: double.infinity,
            child: Text(
              formattedDate,
              style: StyleFont.inter14px600w(Colour.textPrimary),
            ),
          ),
        );
      }
      checklist.add(
        Container(
          padding: EdgeInsets.symmetric(vertical: Value.val5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: Value.val30,
                width: Value.val30,
                child: Checkbox(
                  value: _checklistCheckbox[indx],
                  materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  onChanged: widget.isHRView
                      ? null
                      : (val) async {
                          setState(() {
                            data[indx]["status"] = val;
                            _staffChecklistData = data;
                            getStaffForm();
                          });
                        },
                ),
              ),
              SizedBox(width: Value.val10),
              Expanded(
                child: RichText(
                  text: TextSpan(
                    text: data[i]["task"],
                    style: StyleFont.inter14px400w(
                      Colour.textPrimary,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "\nDurations: " + data[i]["duration"] + " Day(s)",
                        style: StyleFont.inter10px400w(
                          Colour.textSecondary,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );

      // Set global
      setState(() {
        _checklist = checklist;
      });
    }
  }

  // UPDATE STAFF FORM
  Future updateStaffForm() async {
    print(_checklistCheckbox);
    print(_staffChecklistData);

    // Save to static dummy data
    Map rawdata = await Dummy().getData(file.staff);
    rawdata["data"][_index]["data"] = _staffChecklistData;

    String jsonString = json.encode(rawdata);
    Dummy().setData(file.staff, jsonString);

    WidgetDialog().acknowledged(
      context: context,
      title: "Success",
      description: "Update staff form success!",
      titleButton: "Proceed",
      onPressedButton: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      backgroundColor: Colour.bgPrimary,
      body: Stack(
        children: [
          ApplicationBar(
            title: "Staff Form",
            enableBack: true,
          ),
          ContainerBody(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  SizedBox(height: Value.val20),
                  Container(
                    width: Value.val100,
                    height: Value.val100,
                    decoration: BoxDecoration(
                      color: Colour.bgPrimary,
                      borderRadius: BorderRadius.all(
                        Radius.circular(Value.val200),
                      ),
                    ),
                    clipBehavior: Clip.hardEdge,
                    alignment: Alignment.center,
                    child: Image.network(
                      _staffData["profile_url"],
                    ),
                  ),
                  SizedBox(height: Value.val20),
                  Container(
                    alignment: Alignment.centerLeft,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: Value.val30),
                    child: Text(
                      "Staff Name",
                      style: StyleFont.inter14px400w(Colour.textSecondary),
                    ),
                  ),
                  SizedBox(height: Value.val5),
                  Container(
                    alignment: Alignment.centerLeft,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: Value.val30),
                    child: Text(
                      _staffData["name"],
                      style: StyleFont.inter16px600w(Colour.textPrimary),
                    ),
                  ),
                  SizedBox(height: Value.val20),
                  Container(
                    alignment: Alignment.centerLeft,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: Value.val30),
                    child: Text(
                      "Staff Date Start Duty",
                      style: StyleFont.inter14px400w(Colour.textSecondary),
                    ),
                  ),
                  SizedBox(height: Value.val5),
                  Container(
                    alignment: Alignment.centerLeft,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: Value.val30),
                    child: Text(
                      _staffData["date_duty"],
                      style: StyleFont.inter16px600w(Colour.textPrimary),
                    ),
                  ),
                  SizedBox(height: Value.val20),
                  Container(
                    alignment: Alignment.centerLeft,
                    width: double.infinity,
                    padding: EdgeInsets.symmetric(horizontal: Value.val30),
                    child: Text(
                      "Staff Form",
                      style: StyleFont.inter14px400w(Colour.textSecondary),
                    ),
                  ),
                  SizedBox(height: Value.val5),
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: Value.val30),
                    child: DottedBorder(
                      color: Colour.textSecondary,
                      child: Container(
                        padding: EdgeInsets.all(Value.val10),
                        child: Column(
                          children: _checklist,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: Value.val40),
                ],
              ),
            ),
          ),
          Visibility(
            visible: !widget.isHRView,
            child: ButtonBottom(
              text: "Update Staff Form",
              onPressed: updateStaffForm,
            ),
          ),
        ],
      ),
    );
  }
}
