import 'dart:convert';

import 'package:flutter/material.dart';

import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';

import 'style/style_decoration.dart';
import 'style/style_font.dart';
import 'style/style_input.dart';
import 'widget/widget_applicationbar.dart';
import 'widget/widget_buttonbottom.dart';
import 'widget/widget_containerbody.dart';
import 'widget/widget_datepicker.dart';
import 'widget/widget_dialog.dart';
import 'widget/widget_dropdown.dart';
import 'widget/widget_tag.dart';

class ScreenAddStaff extends StatefulWidget {
  @override
  _ScreenAddStaffState createState() => _ScreenAddStaffState();
}

class _ScreenAddStaffState extends State<ScreenAddStaff> {
  // VARIABLE
  // Text input controller properties
  TextEditingController _controllerName = TextEditingController();
  TextEditingController _controllerUsername = TextEditingController();
  TextEditingController _controllerCalendar = TextEditingController();
  // Dropdown for role selection properties
  List<DropdownMenuItem<dynamic>> _dropdownRole = [
    DropdownMenuItem(
      child: Text(
        "Assign role",
        style: StyleFont.inter16px400w(
          Colour.textSecondary,
        ),
      ),
      value: "0",
    ),
  ];
  String _selectedRole = "0";
  // Dropdown for person in charge properties
  List<DropdownMenuItem<dynamic>> _dropdownPIC = [
    DropdownMenuItem(
      child: Text(
        "Select person in charge",
        style: StyleFont.inter16px400w(
          Colour.textSecondary,
        ),
      ),
      value: "0",
    ),
  ];
  String _selectedPIC = "0";
  // List tag person in charge properties
  List<Tag> _listTagPIC = [];
  List<String> _listSelectPIC = [];
  // Create staff properties
  String _lastId = "";

  // INTI STATE
  @override
  void initState() {
    super.initState();
    getListRoles();
    addTagPIC("");
  }

  // GET LIST ROLES FOR DROPDOWN BUTTON
  Future getListRoles() async {
    Map rawdata = await Dummy().getData(file.role);
    List data = rawdata["data"];
    int dataLength = data.length;
    // loop all data
    for (int i = 0; i < dataLength; i++) {
      String roleId = data[i]["id"].toString();
      String roleTitle = data[i]["title"].toString();
      _dropdownRole.add(
        DropdownMenuItem(
            child: Text(
              roleTitle,
              style: StyleFont.inter16px400w(
                Colour.textPrimary,
              ),
            ),
            value: roleId),
      );
    }
  }

  // ADD TAG AFTER PRESS SELECT DROPDOWN
  Future addTagPIC(String selectedPIC) async {
    List<String> listSelectPIC = _listSelectPIC;
    List<Tag> listTagPIC = [];
    String selPIC = selectedPIC == "0" ? "" : selectedPIC;

    // Get staff data
    Map rawdata = await Dummy().getData(file.staff);
    List data = rawdata["data"];
    int dataLength = data.length;
    String lastId = data[dataLength - 1]["id"].toString();
    // List subject
    List<DropdownMenuItem<String>> dropdownPIC = [
      DropdownMenuItem(
          child: Text(
            "Select person in charge",
            style: StyleFont.inter16px400w(
              Colour.textSecondary,
            ),
          ),
          value: "0"),
    ];

    for (int i = 0; i < dataLength; i++) {
      // If selected class data is not null
      if (selPIC != "" && !listSelectPIC.contains(selPIC)) {
        listSelectPIC.add(selPIC);
      }

      // Only put hr and manager inside dropdown
      if (data[i]["role_id"].toString() == "1" ||
          data[i]["role_id"].toString() == "2") {
        dropdownPIC.add(
          DropdownMenuItem(
            child: Text(
              data[i]["name"].toString(),
              style: StyleFont.inter16px400w(
                Colour.textPrimary,
              ),
            ),
            value: data[i]["id"].toString(),
          ),
        );
      }
    }

    // Loop for tag list
    for (int a = 0; a < listSelectPIC.length; a++) {
      for (int i = 0; i < dataLength; i++) {
        String picId = data[i]["id"].toString();
        String picName = data[i]["name"].toString();
        if (listSelectPIC[a].toString() == picId) {
          listTagPIC.add(
            Tag(
              value: picId,
              text: picName,
              bgColor: Colour.bgWarning,
              onTap: () {
                removeTag(picId);
              },
            ),
          );
        }
      }
    }

    // Set global
    setState(() {
      _lastId = lastId;
      _selectedPIC = "0";
      _dropdownPIC = dropdownPIC;
      _listTagPIC = listTagPIC;
      _listSelectPIC = listSelectPIC;
    });
  }

  // REMOVE TAG PIC LIST
  void removeTag(String selectedTag) {
    List<String> listSelectPIC = _listSelectPIC;
    listSelectPIC.remove(selectedTag);
    // Set global
    setState(() {
      _listSelectPIC = listSelectPIC;
    });
    // Set latest tag
    addTagPIC("");
  }

  // ADD NEW STAFF TO DATABASE
  Future addNewStaff() async {
    FocusScope.of(context).requestFocus(new FocusNode());
    // Validate form
    if (_controllerName.text == "" || _controllerName.text == null) {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please fill staff name',
        titleButton: 'Proceed',
      );
      return;
    }
    if (_controllerUsername.text == "" || _controllerUsername.text == null) {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please fill staff username',
        titleButton: 'Proceed',
      );
      return;
    }
    if (_controllerCalendar.text == "" || _controllerCalendar.text == null) {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please select staff date start duty',
        titleButton: 'Proceed',
      );
      return;
    }
    if (_selectedRole == "0") {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please select staff role',
        titleButton: 'Proceed',
      );
      return;
    }
    if (_listSelectPIC.length <= 0) {
      WidgetDialog().error(
        context: context,
        title: 'Error',
        description: 'Please select at least one person in charge',
        titleButton: 'Proceed',
      );
      return;
    }

    // Initialize form variable
    String formId = "0";
    List formData = [];
    int newId = int.parse(_lastId);
    newId += 1;

    // Find form attached with selected role
    Map rawdata = await Dummy().getData(file.role);
    List data = rawdata["data"];
    int dataLength = data.length;
    for (int i = 0; i < dataLength; i++) {
      if (data[i]["id"] == _selectedRole) {
        formId = data[i]["form_id"].toString();
        print("Form ID: " + formId);
        // End loop
        i = dataLength;
      }
    }

    // Find form data with form id
    rawdata = await Dummy().getData(file.form);
    data = rawdata["data"];
    dataLength = data.length;
    for (int i = 0; i < dataLength; i++) {
      if (data[i]["id"] == formId) {
        formData = data[i]["data"];
        print("Form Data:" + formData.toString());
        // End loop
        i = dataLength;
      }
    }

    // Create user profile data
    String id = newId.toString();
    String name = _controllerName.text;
    String username = _controllerUsername.text;
    String password = "1234";
    String roleId = _selectedRole;
    List picIds = _listSelectPIC;
    String dateDuty = _controllerCalendar.text;
    String profileURL = "https://randomuser.me/api/portraits/men/$newId.jpg";

    // Save to static dummy data
    rawdata = await Dummy().getData(file.staff);
    List staffList = rawdata["data"];
    staffList.add({
      "id": id,
      "name": name,
      "username": username,
      "password": password,
      "role_id": roleId,
      "pic_ids": picIds,
      "profile_url": profileURL,
      "date_duty": dateDuty,
      "data": formData,
    });
    Map staffData = {"data": staffList};
    String jsonString = json.encode(staffData);
    Dummy().setData(file.staff, jsonString);

    WidgetDialog().acknowledged(
      context: context,
      title: "Success",
      description: "New staff successfuly created!",
      titleButton: "Proceed",
      onPressedButton: () {
        Navigator.pop(context);
        Navigator.pop(context);
      },
    );
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      backgroundColor: Colour.bgPrimary,
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: [
            ApplicationBar(
              title: "Add New Staff",
              enableBack: true,
            ),
            ContainerBody(
              child: SingleChildScrollView(
                child: Column(
                  children: [
                    SizedBox(height: Value.val30),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Name",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Container(
                      decoration: StyleDecoration.basic(),
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: TextFormField(
                        controller: _controllerName,
                        style: StyleFont.inter16px400w(
                          Colour.textPrimary,
                        ),
                        keyboardType: TextInputType.text,
                        decoration: StyleInput().border(
                          hintText: '',
                          hintStyle: StyleFont.inter14px400w(
                            Colour.textSecondary,
                          ),
                          fillColor: Colour.white,
                          borderColor: Colour.transparent,
                          borderThick: 0,
                          radius: 10,
                        ),
                      ),
                    ),
                    SizedBox(height: Value.val20),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Username",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Container(
                      decoration: StyleDecoration.basic(),
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: TextFormField(
                        controller: _controllerUsername,
                        style: StyleFont.inter16px400w(
                          Colour.textPrimary,
                        ),
                        keyboardType: TextInputType.text,
                        decoration: StyleInput().border(
                          hintText: '',
                          hintStyle: StyleFont.inter14px400w(
                            Colour.textSecondary,
                          ),
                          fillColor: Colour.white,
                          borderColor: Colour.transparent,
                          borderThick: 0,
                          radius: 10,
                        ),
                      ),
                    ),
                    SizedBox(height: Value.val20),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Date Start Duty",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Container(
                      height: Value.val60,
                      width: double.infinity,
                      margin: EdgeInsets.symmetric(horizontal: Value.val30),
                      decoration: BoxDecoration(
                        color: Colour.white,
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          Container(
                            child: Row(
                              children: [
                                SizedBox(width: Value.val10),
                                Container(
                                  // width: Value.val100,
                                  child: DatePicker(
                                    title: "Date Start Duty",
                                    firstDate: DateTime(2022),
                                    lastDate: DateTime(2032),
                                    dateController: _controllerCalendar,
                                    onChanged: (val) {
                                      print("$val");
                                    },
                                    enableWorkingDays: false,
                                    listWorkDays: [],
                                  ),
                                ),
                                SizedBox(width: Value.val10),
                              ],
                            ),
                          ),
                        ],
                      ),
                    ),
                    SizedBox(height: Value.val20),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Role",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: Value.val30,
                      ),
                      child: DropDown(
                        dropdownItems: _dropdownRole,
                        selectedItem: _selectedRole,
                        onChanged: (selected) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          setState(() {
                            _selectedRole = selected;
                          });
                        },
                      ),
                    ),
                    SizedBox(height: Value.val20),
                    Container(
                      alignment: Alignment.centerLeft,
                      width: double.infinity,
                      padding: EdgeInsets.symmetric(horizontal: Value.val30),
                      child: Text(
                        "Person In Charge",
                        style: StyleFont.inter14px600w(Colour.textSecondary),
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Visibility(
                      visible: (_listTagPIC.length > 0),
                      child: Container(
                        alignment: Alignment.topLeft,
                        margin: EdgeInsets.symmetric(horizontal: Value.val30),
                        child: Wrap(spacing: 2, children: _listTagPIC),
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.symmetric(
                        horizontal: Value.val30,
                      ),
                      child: DropDown(
                        dropdownItems: _dropdownPIC,
                        selectedItem: _selectedPIC,
                        onChanged: (selected) {
                          FocusScope.of(context).requestFocus(new FocusNode());
                          addTagPIC(selected);
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
            ButtonBottom(
              text: "Add New Staff",
              onPressed: addNewStaff,
            ),
          ],
        ),
      ),
    );
  }
}
