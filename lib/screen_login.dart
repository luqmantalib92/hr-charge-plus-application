import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'data/data_cache.dart';
import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_appbar.dart';
import 'style/style_color.dart';
import 'style/style_decoration.dart';
import 'style/style_font.dart';
import 'style/style_input.dart';

import 'widget/widget_dialog.dart';

import 'screen_hrstaff.dart';
import 'screen_managerstaff.dart';

class ScreenLogin extends StatefulWidget {
  @override
  _ScreenLoginState createState() => _ScreenLoginState();
}

class _ScreenLoginState extends State<ScreenLogin> {
  // VARIABLE
  // Form controller properties
  TextEditingController _controllerUsername = TextEditingController();
  TextEditingController _controllerPassword = TextEditingController();

  // SIGN IN USER
  Future signIn() async {
    // hide keyboard input
    FocusScope.of(context).requestFocus(new FocusNode());
    // Initialize local input
    String username = _controllerUsername.text;
    String password = _controllerPassword.text;
    // Get data from dummy
    Map rawdata = await Dummy().getData(file.staff);
    List data = rawdata["data"];
    int dataLength = data.length;
    bool isLoggedIn = false;
    bool isPermitted = false;
    // Loop all data to check username and password match
    for (int i = 0; i < dataLength; i++) {
      if (data[i]["username"] == username) {
        if (data[i]["password"] == password) {
          print("Sign in success!");

          // Save user login data to cache
          Cache.id = data[i]["id"];
          Cache.name = data[i]["name"];
          Cache.roleid = data[i]["role_id"];
          Cache.picids = data[i]["pic_ids"];
          Cache.profileurl = data[i]["profile_url"];

          // Check user role
          if (data[i]["role_id"] == "1") {
            print("Load HR page");
            // Clear input
            _controllerUsername.clear();
            _controllerPassword.clear();
            // Set permission
            isLoggedIn = true;
            isPermitted = true;
            Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.fade,
                child: ScreenHRStaff(),
              ),
            );
            break;
          } else if (data[i]["role_id"] == "2") {
            print("Load Manager page");
            // Clear input
            _controllerUsername.clear();
            _controllerPassword.clear();
            // Set permission
            isLoggedIn = true;
            isPermitted = true;
            Navigator.pushReplacement(
              context,
              PageTransition(
                type: PageTransitionType.fade,
                child: ScreenManagerStaff(),
              ),
            );
          } else {
            isLoggedIn = true;
            isPermitted = false;
            print("Load Staff page");
          }
        } else {
          print("Password entered is incorrect!");
        }
        // Break the loop
        i = dataLength;
      } else {
        print("User not found in database!");
      }
    }

    // Check login status
    if (!isLoggedIn && !isPermitted) {
      WidgetDialog().error(
        context: context,
        title: "Error",
        description: "Username or password is incorrect!",
        titleButton: "Retry",
      );
    } else if (isLoggedIn && !isPermitted) {
      WidgetDialog().error(
        context: context,
        title: "Error",
        description: "User don't have permission to access!",
        titleButton: "Retry",
      );
    }
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: StyleAppBar.transparent(),
      body: GestureDetector(
        onTap: () {
          FocusScope.of(context).requestFocus(new FocusNode());
        },
        child: Stack(
          children: [
            Container(
              height: double.infinity,
              width: double.infinity,
              color: Colour.bgScaffold,
            ),
            Align(
              alignment: Alignment.topCenter,
              child: SingleChildScrollView(
                child: Container(
                  width: 450,
                  child: Padding(
                    padding: EdgeInsets.symmetric(horizontal: Value.val60),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(height: Value.val100),
                        Align(
                          alignment: Alignment.center,
                          child: Container(
                            width: Value.val100,
                            child: Image.asset("assets/logo.png"),
                          ),
                        ),
                        Container(
                          alignment: Alignment.center,
                          width: double.infinity,
                          child: Text(
                            "Enter your details below",
                            style: StyleFont.inter18px600w(Colors.black),
                          ),
                        ),

                        SizedBox(height: Value.val20),
                        Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              // Input area username
                              SizedBox(height: Value.val30),
                              Container(
                                alignment: Alignment.centerLeft,
                                width: double.infinity,
                                child: Text(
                                  "Username",
                                  style: StyleFont.inter14px600w(Colors.black),
                                ),
                              ),
                              SizedBox(height: Value.val5),
                              Container(
                                decoration: StyleDecoration.basic(),
                                child: TextFormField(
                                  controller: _controllerUsername,
                                  style: StyleFont.inter16px400w(
                                    Colour.textSecondary,
                                  ),
                                  keyboardType: TextInputType.text,
                                  decoration: StyleInput().border(
                                    hintText: '',
                                    hintStyle: StyleFont.inter14px400w(
                                      Colour.textSecondary,
                                    ),
                                    fillColor: Colour.bgInput,
                                    borderColor: Colour.borderInput,
                                    borderThick: 2,
                                    radius: 5,
                                  ),
                                ),
                              ),

                              // Input area password
                              SizedBox(height: Value.val20),
                              Container(
                                alignment: Alignment.centerLeft,
                                width: double.infinity,
                                child: Text(
                                  "Password",
                                  style: StyleFont.inter14px600w(Colors.black),
                                ),
                              ),
                              SizedBox(height: Value.val5),
                              Container(
                                decoration: StyleDecoration.basic(),
                                child: TextFormField(
                                  controller: _controllerPassword,
                                  style: StyleFont.inter16px400w(
                                    Colour.textSecondary,
                                  ),
                                  keyboardType: TextInputType.visiblePassword,
                                  obscureText: true,
                                  decoration: StyleInput().border(
                                    hintText: '',
                                    hintStyle: StyleFont.inter14px400w(
                                      Colour.textSecondary,
                                    ),
                                    fillColor: Colour.bgInput,
                                    borderColor: Colour.borderInput,
                                    borderThick: 2,
                                    radius: 5,
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),

                        // Button sign in
                        SizedBox(height: Value.val30),
                        Container(
                          height: Value.val56,
                          width: double.infinity,
                          child: FlatButton(
                            color: Colour.bgPrimary,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(30),
                            ),
                            child: Text(
                              "Sign In",
                              style: StyleFont.inter14px600w(Colors.white),
                            ),
                            onPressed: signIn,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
