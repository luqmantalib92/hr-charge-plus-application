import 'dart:async';

import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import 'data/data_dummy.dart';
import 'data/data_value.dart';

import 'style/style_font.dart';
import 'style/style_color.dart';

import 'screen_login.dart';

class ScreenSplash extends StatefulWidget {
  @override
  _ScreenSplashState createState() => _ScreenSplashState();
}

class _ScreenSplashState extends State<ScreenSplash> {
  // VARIABLE
  // Loading bar properties
  int _speedLoading = 3; /* The higher the value, the slower */
  double _bgWidthLoadingBar = Value.val220;
  double _fgWidthLoadingBar = 0;
  Timer _timer;

  // INIT STATE
  @override
  void initState() {
    super.initState();
    initFunction();
  }

  // INIT FUNCTION
  Future initFunction() async {
    await Dummy().createFileApplication(context);
    await animateLoading();
  }

  // ANIMATE LOADING
  Future animateLoading() async {
    // start timer
    _timer = Timer.periodic(Duration(milliseconds: _speedLoading), (Timer t) {
      setState(() {
        // expand loading bar width size
        _fgWidthLoadingBar++;
        // if fully load, execute next action
        if (_fgWidthLoadingBar > _bgWidthLoadingBar) {
          // stop timer
          _timer.cancel();
          // splash screen replace with login screen.

          Navigator.pushReplacement(
            context,
            PageTransition(
              type: PageTransitionType.fade,
              child: ScreenLogin(),
            ),
          );
        }
      });
    });
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    // Get the screen size
    Size size = MediaQuery.of(context).size;
    Value.screenHeight = size.height;
    Value.screenWidth = size.width;
    // Set the size component based on screen width
    if (size.width < 400) Value.initGlobalSize(0.8);

    return Scaffold(
      backgroundColor: Colour.bgScaffold,
      body: Stack(
        children: [
          // Image application logo
          Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                  margin: EdgeInsets.symmetric(horizontal: Value.val60),
                  width: Value.val200,
                  child: Image.asset("assets/logo.png"),
                ),
                Container(height: Value.val140),
              ],
            ),
          ),

          // Label application copyright
          Container(
            width: double.infinity,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Container(
                  alignment: Alignment.center,
                  width: double.infinity,
                  child: Stack(
                    alignment: Alignment.centerLeft,
                    children: [
                      Container(
                        height: Value.val10,
                        width: _bgWidthLoadingBar,
                        decoration: BoxDecoration(
                          color: Colour.bgTertiary,
                          borderRadius: BorderRadius.circular(Value.val30),
                        ),
                      ),
                      Container(
                        height: Value.val10,
                        width: _fgWidthLoadingBar,
                        decoration: BoxDecoration(
                          color: Colour.bgPrimary,
                          borderRadius: BorderRadius.circular(Value.val30),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: Value.val30),
                Text(
                  'Copyright 2022 © CHARGE+',
                  style: StyleFont.inter10px600w(Colour.textSecondary),
                ),
                SizedBox(height: Value.val40),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
