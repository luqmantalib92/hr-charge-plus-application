import 'package:flutter/material.dart';

import '../data/data_value.dart';

import 'style_color.dart';

class StyleAppBar {
  static PreferredSize transparent() {
    return PreferredSize(
      preferredSize: Size.fromHeight(0),
      child: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: Colour.bgPrimary,
      ),
    );
  }

  static PreferredSize basic(context) {
    return PreferredSize(
      preferredSize: Size.fromHeight(Value.val60),
      child: AppBar(
        shadowColor: Colors.transparent,
        backgroundColor: Colour.bgPrimary,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            size: Value.val30,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
    );
  }
}
