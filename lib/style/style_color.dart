import 'package:flutter/material.dart';

class Colour {
  // Basic
  static Color transparent = Colors.transparent;
  static Color black = Colors.black;
  static Color white = Colors.white;
  static Color white60 = Colors.white60;

  // Text
  static Color textPrimary = Color.fromRGBO(19, 28, 49, 1);
  static Color textSecondary = Color.fromRGBO(120, 136, 160, 1);
  static Color textTertiary = Color.fromRGBO(50, 53, 73, 1);
  static Color textHighlight = Color.fromRGBO(0, 158, 247, 1);
  static Color textHighlight50 = Color.fromRGBO(0, 158, 247, 0.5);

  // Background
  static Color bgScaffold = Color.fromRGBO(242, 245, 248, 1);
  static Color bgPrimary = Color.fromRGBO(80, 150, 77, 1);
  static Color bgSecondary = Color.fromRGBO(59, 110, 57, 1);
  static Color bgTertiary = Color.fromRGBO(20, 70, 20, 1);
  static Color bgDefault = Color.fromRGBO(0, 158, 247, 1);
  static Color bgWarning = Color.fromRGBO(255, 199, 0, 1);
  static Color bgSuccess = Color.fromRGBO(80, 205, 137, 1);
  static Color bgAlert = Color.fromRGBO(241, 65, 108, 1);
  static Color bgError = Color.fromRGBO(176, 0, 32, 1);
  static Color bgInput = Color.fromRGBO(247, 248, 252, 1);
  static Color bgMedia = Color.fromRGBO(229, 229, 229, 1);

  // Foreground
  static Color fgLoading = Color.fromRGBO(255, 60, 0, 1);

  // Shadow
  static Color shadow1 = Color.fromRGBO(0, 0, 0, 0.01);
  static Color shadow2 = Color.fromRGBO(0, 0, 0, 0.02);

  // Border
  static Color borderInput = Color.fromRGBO(230, 235, 242, 1);

  // Icon
  static Color iconFade = Color.fromRGBO(229, 229, 229, 1);
}
