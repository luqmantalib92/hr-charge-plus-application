import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

import '../data/data_value.dart';

class StyleFont {
  // INTER
  static TextStyle inter36px600w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val36,
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle inter34px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val34,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter24px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val24,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter24px600w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val24,
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle inter20px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val20,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter20px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val20,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }

  static TextStyle inter18px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val18,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter18px600w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val18,
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle inter18px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val18,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }

  static TextStyle inter16px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val16,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter16px600w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val16,
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle inter16px500w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val16,
      fontWeight: FontWeight.w500,
      color: color,
    );
  }

  static TextStyle inter16px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val16,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }

  static TextStyle inter15px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val15,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter14px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val14,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter14px600w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val14,
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle inter14px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val14,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }

  static TextStyle inter13px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val13,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }

  static TextStyle inter12px700w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val12,
      fontWeight: FontWeight.w700,
      color: color,
    );
  }

  static TextStyle inter12px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val12,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }

  static TextStyle inter10px600w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val10,
      fontWeight: FontWeight.w600,
      color: color,
    );
  }

  static TextStyle inter10px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val10,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }

  static TextStyle inter8px400w(Color color) {
    return GoogleFonts.inter(
      fontSize: Value.val8,
      fontWeight: FontWeight.w400,
      color: color,
    );
  }
}
