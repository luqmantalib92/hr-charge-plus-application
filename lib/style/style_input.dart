import 'package:flutter/material.dart';

import '../data/data_value.dart';

import 'style_color.dart';

class StyleInput {
  // Decoration box
  InputDecoration border({
    String hintText,
    TextStyle hintStyle,
    Color fillColor,
    Color borderColor,
    double borderThick,
    double radius,
    Widget prefixIcon,
  }) {
    return InputDecoration(
      filled: true,
      fillColor: fillColor,
      hintText: hintText,
      hintStyle: hintStyle,
      prefixIcon: prefixIcon,
      prefixIconConstraints: BoxConstraints(
        minHeight: 10,
        minWidth: 10,
      ),
      contentPadding: EdgeInsets.all(Value.val15),
      errorStyle: TextStyle(
        fontSize: Value.val10,
        color: Colour.bgError,
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide(color: borderColor, width: borderThick),
        borderRadius: BorderRadius.circular(radius),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: borderColor, width: borderThick),
        borderRadius: BorderRadius.circular(radius),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: borderColor, width: borderThick),
        borderRadius: BorderRadius.circular(radius),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colour.bgAlert, width: borderThick),
        borderRadius: BorderRadius.circular(radius),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colour.bgAlert, width: borderThick),
        borderRadius: BorderRadius.circular(radius),
      ),
    );
  }

  // Decoration box
  InputDecoration empty({
    String hintText,
    TextStyle hintStyle,
  }) {
    return InputDecoration(
      filled: true,
      fillColor: Colors.transparent,
      hintText: hintText,
      hintStyle: hintStyle,
      prefixIconConstraints: BoxConstraints(
        minHeight: 10,
        minWidth: 10,
      ),
      contentPadding: EdgeInsets.all(0),
      errorStyle: TextStyle(
        fontSize: Value.val10,
        color: Colour.bgError,
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colors.transparent, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colour.bgAlert, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colour.bgAlert, width: 0),
        borderRadius: BorderRadius.circular(0),
      ),
    );
  }

  // Decoration box
  InputDecoration basic({
    String hintText,
    TextStyle hintTextStyle,
    Color underlineColor,
  }) {
    return InputDecoration(
      filled: false,
      hintText: hintText,
      hintStyle: hintTextStyle,
      contentPadding: EdgeInsets.symmetric(
        vertical: Value.val15,
        horizontal: 0,
      ),
      errorStyle: TextStyle(
        fontSize: Value.val10,
        color: Colour.bgError,
      ),
      enabledBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: underlineColor),
      ),
      focusedBorder: UnderlineInputBorder(
        borderSide: BorderSide(color: underlineColor),
      ),
    );
  }

  // Decoration dialog
  InputDecoration dialog({
    String hintText,
    TextStyle hintTextStyle,
    Color fillColor,
    double radius,
    double contentPadding,
  }) {
    return InputDecoration(
      filled: true,
      fillColor: fillColor,
      hintText: hintText,
      hintStyle: hintTextStyle,
      contentPadding: EdgeInsets.all(contentPadding),
      errorStyle: TextStyle(
        fontSize: Value.val10,
        color: Colour.bgError,
      ),
      border: OutlineInputBorder(
        borderSide: BorderSide(color: fillColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      focusedBorder: OutlineInputBorder(
        borderSide: BorderSide(color: fillColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      enabledBorder: OutlineInputBorder(
        borderSide: BorderSide(color: fillColor),
        borderRadius: BorderRadius.circular(radius),
      ),
      errorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colour.bgAlert),
        borderRadius: BorderRadius.circular(radius),
      ),
      focusedErrorBorder: OutlineInputBorder(
        borderSide: BorderSide(color: Colour.bgAlert),
        borderRadius: BorderRadius.circular(radius),
      ),
    );
  }
}
