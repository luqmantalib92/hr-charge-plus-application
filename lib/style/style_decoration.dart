import 'package:flutter/material.dart';

import 'style_color.dart';

class StyleDecoration {
  static BoxDecoration empty() {
    return BoxDecoration();
  }

  static BoxDecoration basic() {
    return BoxDecoration(
      boxShadow: [
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 8,
          offset: Offset(0, 3),
        ),
      ],
    );
  }

  static BoxDecoration cardBorder({Color color, double radius}) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(radius),
      border: Border.all(width: 1, color: Colour.bgDefault),
      boxShadow: [
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 1.54,
          offset: Offset(0, 0.66),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 2.85,
          offset: Offset(0, 1.58),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 4.06,
          offset: Offset(0, 3.03),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 5.83,
          offset: Offset(0, 5.69),
        ),
        BoxShadow(
          color: Colour.shadow2,
          blurRadius: 14,
          offset: Offset(0, 12),
        ),
      ],
    );
  }

  static BoxDecoration card({Color color, double radius}) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(radius),
      boxShadow: [
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 1.54,
          offset: Offset(0, 0.66),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 2.85,
          offset: Offset(0, 1.58),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 4.06,
          offset: Offset(0, 3.03),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 5.83,
          offset: Offset(0, 5.69),
        ),
        BoxShadow(
          color: Colour.shadow2,
          blurRadius: 14,
          offset: Offset(0, 12),
        ),
      ],
    );
  }

  static BoxDecoration bottom({Color color, double radius}) {
    return BoxDecoration(
      color: color,
      borderRadius: BorderRadius.circular(radius),
      boxShadow: [
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 1.54,
          offset: Offset(0, -0.66),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 2.85,
          offset: Offset(0, -1.58),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 4.06,
          offset: Offset(0, -3.03),
        ),
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 5.83,
          offset: Offset(0, -5.69),
        ),
        BoxShadow(
          color: Colour.shadow2,
          blurRadius: 14,
          offset: Offset(0, -12),
        ),
      ],
    );
  }

  static BoxDecoration date() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
      boxShadow: [
        BoxShadow(
          color: Colour.shadow1,
          blurRadius: 8,
          offset: Offset(0, 3),
        ),
      ],
    );
  }

  static BoxDecoration emptyDate() {
    return BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.all(Radius.circular(10)),
    );
  }
}
