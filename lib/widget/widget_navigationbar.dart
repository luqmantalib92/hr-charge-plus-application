import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../data/data_value.dart';

import '../style/style_font.dart';
import '../style/style_color.dart';

import '../screen_hrstaff.dart';
import '../screen_hrform.dart';
import '../screen_hrrole.dart';
import '../screen_hrsetting.dart';
import '../screen_managerstaff.dart';
import '../screen_managersetting.dart';

enum roles { hr, manager }

class NavigationBar extends StatelessWidget {
  final int index;
  final roles role;
  NavigationBar({@required this.index, @required this.role});

  @override
  Widget build(BuildContext context) {
    switch (role) {
      case roles.hr:
        return Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
              color: Colour.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(Value.val20),
                topRight: Radius.circular(Value.val20),
              ),
            ),
            height: Value.val70,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // Navigation button to staff page
                GestureDetector(
                  onTap: () {
                    if (index != 0) {
                      Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ScreenHRStaff(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    }
                  },
                  child: Container(
                    width: Value.val70,
                    color: Colour.white,
                    child: Column(
                      children: [
                        SizedBox(height: Value.val10),
                        ImageIcon(
                          Image.asset("assets/staff.png").image,
                          color:
                              index == 0 ? Colour.bgPrimary : Colour.bgTertiary,
                          size: Value.val24,
                        ),
                        SizedBox(height: Value.val5),
                        Text(
                          "Staff",
                          style: StyleFont.inter12px400w(
                            index == 0 ? Colour.bgPrimary : Colour.bgTertiary,
                          ),
                        )
                      ],
                    ),
                  ),
                ),

                // Navigation button to role page
                GestureDetector(
                  onTap: () {
                    if (index != 1) {
                      Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ScreenHRRole(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    }
                  },
                  child: Container(
                    width: Value.val70,
                    color: Colour.white,
                    child: Column(
                      children: [
                        SizedBox(height: Value.val10),
                        ImageIcon(
                          Image.asset("assets/role.png").image,
                          color:
                              index == 1 ? Colour.bgPrimary : Colour.bgTertiary,
                          size: Value.val24,
                        ),
                        SizedBox(height: Value.val5),
                        Text(
                          "Role",
                          style: StyleFont.inter12px400w(
                            index == 1 ? Colour.bgPrimary : Colour.bgTertiary,
                          ),
                        )
                      ],
                    ),
                  ),
                ),

                // Navigation button to form page
                GestureDetector(
                  onTap: () {
                    if (index != 2) {
                      Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ScreenHRForm(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    }
                  },
                  child: Container(
                    width: Value.val70,
                    color: Colour.white,
                    child: Column(
                      children: [
                        SizedBox(height: Value.val10),
                        ImageIcon(
                          Image.asset("assets/form.png").image,
                          color:
                              index == 2 ? Colour.bgPrimary : Colour.bgTertiary,
                          size: Value.val24,
                        ),
                        SizedBox(height: Value.val5),
                        Text(
                          "Form",
                          style: StyleFont.inter12px400w(
                            index == 2 ? Colour.bgPrimary : Colour.bgTertiary,
                          ),
                        )
                      ],
                    ),
                  ),
                ),

                // Navigation button to setting page
                GestureDetector(
                  onTap: () {
                    if (index != 3) {
                      Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ScreenHRSetting(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    }
                  },
                  child: Container(
                    width: Value.val70,
                    color: Colour.white,
                    child: Column(
                      children: [
                        SizedBox(height: Value.val10),
                        ImageIcon(
                          Image.asset("assets/setting.png").image,
                          color:
                              index == 3 ? Colour.bgPrimary : Colour.bgTertiary,
                          size: Value.val24,
                        ),
                        SizedBox(height: Value.val5),
                        Text(
                          "Setting",
                          style: StyleFont.inter12px400w(
                            index == 3 ? Colour.bgPrimary : Colour.bgTertiary,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
        break;
      case roles.manager:
        return Align(
          alignment: Alignment.bottomCenter,
          child: Container(
            decoration: BoxDecoration(
              color: Colour.white,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(Value.val20),
                topRight: Radius.circular(Value.val20),
              ),
            ),
            height: Value.val70,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              children: [
                // Navigation button to staff page
                GestureDetector(
                  onTap: () {
                    if (index != 0) {
                      Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ScreenManagerStaff(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    }
                  },
                  child: Container(
                    width: Value.val70,
                    color: Colour.white,
                    child: Column(
                      children: [
                        SizedBox(height: Value.val10),
                        ImageIcon(
                          Image.asset("assets/staff.png").image,
                          color:
                              index == 0 ? Colour.bgPrimary : Colour.bgTertiary,
                          size: Value.val24,
                        ),
                        SizedBox(height: Value.val5),
                        Text(
                          "Staff",
                          style: StyleFont.inter12px400w(
                            index == 0 ? Colour.bgPrimary : Colour.bgTertiary,
                          ),
                        )
                      ],
                    ),
                  ),
                ),

                // Navigation button to setting page
                GestureDetector(
                  onTap: () {
                    if (index != 1) {
                      Navigator.pushAndRemoveUntil(
                        context,
                        PageTransition(
                          type: PageTransitionType.fade,
                          child: ScreenManagerSetting(),
                        ),
                        ModalRoute.withName('/'),
                      );
                    }
                  },
                  child: Container(
                    width: Value.val70,
                    color: Colour.white,
                    child: Column(
                      children: [
                        SizedBox(height: Value.val10),
                        ImageIcon(
                          Image.asset("assets/setting.png").image,
                          color:
                              index == 1 ? Colour.bgPrimary : Colour.bgTertiary,
                          size: Value.val24,
                        ),
                        SizedBox(height: Value.val5),
                        Text(
                          "Setting",
                          style: StyleFont.inter12px400w(
                            index == 1 ? Colour.bgPrimary : Colour.bgTertiary,
                          ),
                        )
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
        break;
      default:
        return Container();
        break;
    }
  }
}
