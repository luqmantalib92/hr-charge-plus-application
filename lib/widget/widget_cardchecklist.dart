import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';

class CardChecklist extends StatelessWidget {
  final String title;
  final Function onPressed;
  CardChecklist({
    @required this.title,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: Value.val30,
        vertical: Value.val5,
      ),
      child: FlatButton(
        color: Colour.bgMedia,
        padding: EdgeInsets.all(Value.val15),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(left: Value.val15),
                child: Text(
                  "$title",
                  style: StyleFont.inter16px400w(Colour.textPrimary),
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.only(right: Value.val10),
              child: Icon(
                Icons.edit_rounded,
                size: Value.val25,
              ),
            ),
          ],
        ),
        onPressed: onPressed,
      ),
    );
  }
}
