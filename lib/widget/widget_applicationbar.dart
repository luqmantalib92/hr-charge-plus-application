import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_font.dart';
import '../style/style_color.dart';

import '../widget/widget_buttoninvisible.dart';

class ApplicationBar extends StatelessWidget {
  final String title;
  final bool enableBack;
  ApplicationBar({
    @required this.title,
    this.enableBack = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Value.val100,
      alignment: Alignment.center,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          ButtonInvisible(
            padding: EdgeInsets.symmetric(
              vertical: Value.val15,
              horizontal: Value.val25,
            ),
            child: Image.asset(
              "assets/arrowleft.png",
              width: Value.val20,
              color: enableBack ? Colour.white : Colour.transparent,
            ),
            onPressed: enableBack
                ? () {
                    Navigator.pop(context);
                  }
                : null,
          ),
          Text(
            "$title",
            style: StyleFont.inter20px700w(Colour.white),
          ),
          ButtonInvisible(
            padding: EdgeInsets.symmetric(
              vertical: Value.val15,
              horizontal: Value.val25,
            ),
            child: Image.asset(
              "assets/arrowleft.png",
              width: Value.val20,
              color: Colour.transparent,
            ),
            onPressed: null,
          ),
        ],
      ),
    );
  }
}
