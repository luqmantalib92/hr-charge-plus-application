import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_decoration.dart';
import '../style/style_font.dart';

class DropDown extends StatelessWidget {
  final String type;
  final List<DropdownMenuItem<dynamic>> dropdownItems;
  final String selectedItem;
  final Function onChanged;
  DropDown({
    this.type = "Basic",
    @required this.dropdownItems,
    @required this.selectedItem,
    @required this.onChanged,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: StyleDecoration.basic(),
      child: DropdownButtonFormField(
        isDense: true,
        isExpanded: true,
        items: dropdownItems,
        value: selectedItem,
        onChanged: (selected) {
          onChanged(selected);
        },
        style: StyleFont.inter16px500w(Colors.black),
        iconSize: Value.val15,
        icon: ImageIcon(
          AssetImage("assets/arrowdrop.png"),
          color: Colors.black,
        ),
        decoration: InputDecoration(
          filled: true,
          fillColor: Colors.white,
          contentPadding: EdgeInsets.symmetric(
            vertical: Value.val15,
            horizontal: Value.val20,
          ),
          border: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(10),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colors.white),
            borderRadius: BorderRadius.circular(10),
          ),
          errorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colour.bgError),
            borderRadius: BorderRadius.circular(10),
          ),
          focusedErrorBorder: OutlineInputBorder(
            borderSide: BorderSide(color: Colour.bgError),
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ),
    );
  }
}
