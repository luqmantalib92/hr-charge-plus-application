import 'package:flutter/material.dart';

import '../style/style_font.dart';

import '../data/data_value.dart';

class ButtonBorder extends StatelessWidget {
  final String text;
  final Color textColor;
  final Color backgroundColor;
  final Color borderColor;
  final Function onPressed;
  final bool isIcon;
  final AssetImage icon;
  final double iconSize;
  final Color iconColor;
  ButtonBorder({
    @required this.text,
    @required this.textColor,
    @required this.backgroundColor,
    @required this.borderColor,
    @required this.onPressed,
    @required this.isIcon,
    this.icon,
    this.iconSize,
    this.iconColor,
  });
  @override
  Widget build(BuildContext context) {
    // ICON
    if (isIcon) {
      return Container(
        height: Value.val50,
        width: Value.val50,
        child: FlatButton(
          padding: EdgeInsets.all(0),
          materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
          color: backgroundColor,
          child: icon != null
              ? Image(
                  image: icon,
                  color: iconColor,
                  width: iconSize,
                )
              : Image(
                  image: AssetImage("assets/bell.png"),
                  color: Colors.white,
                  width: Value.val20,
                ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: BorderSide(
              width: 1.5,
              color: borderColor,
            ),
          ),
          onPressed: onPressed,
        ),
      );
    }
    // TEXT
    else {
      return Container(
        height: Value.val50,
        width: double.infinity,
        child: FlatButton(
          color: backgroundColor,
          child: Text(
            text,
            style: StyleFont.inter15px700w(textColor),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
            side: BorderSide(
              width: 1.5,
              color: borderColor,
            ),
          ),
          onPressed: onPressed,
        ),
      );
    }
  }
}
