import 'package:flutter/material.dart';

class ButtonInvisible extends StatelessWidget {
  final Widget child;
  final EdgeInsetsGeometry padding;
  final Function onPressed;
  ButtonInvisible({
    @required this.child,
    @required this.padding,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return ButtonTheme(
      minWidth: 0,
      height: 0,
      padding: padding,
      materialTapTargetSize: MaterialTapTargetSize.shrinkWrap,
      child: FlatButton(
        onPressed: onPressed,
        child: child,
      ),
    );
  }
}
