import 'package:flutter/material.dart';
import 'package:date_time_picker/date_time_picker.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';
import '../style/style_input.dart';

class DatePicker extends StatefulWidget {
  final bool readOnly;
  final String title;
  final DateTime firstDate;
  final DateTime lastDate;
  final TextEditingController dateController;
  final Function onChanged;
  final bool enableWorkingDays;
  final List<int> listWorkDays;
  DatePicker({
    this.readOnly = false,
    @required this.title,
    @required this.firstDate,
    @required this.lastDate,
    @required this.dateController,
    @required this.onChanged,
    @required this.enableWorkingDays,
    @required this.listWorkDays,
  });

  @override
  _DatePicker createState() => _DatePicker();
}

class _DatePicker extends State<DatePicker> {
  @override
  Widget build(BuildContext context) {
    return Expanded(
      child: Container(
        height: Value.val60,
        width: double.infinity,
        child: Row(
          children: [
            SizedBox(width: Value.val10),
            Icon(
              Icons.calendar_today,
              size: Value.val24,
              color: Colour.textSecondary,
            ),
            Expanded(
              child: Container(
                width: Value.val200,
                child: DateTimePicker(
                  readOnly: widget.readOnly,
                  type: DateTimePickerType.date,
                  dateMask: 'MMMM dd, yyyy',
                  style: StyleFont.inter16px400w(Colour.textSecondary),
                  controller: widget.dateController,
                  firstDate: widget.firstDate,
                  lastDate: widget.lastDate,
                  decoration: StyleInput().border(
                    hintText: widget.title,
                    hintStyle: StyleFont.inter16px400w(Colour.textSecondary),
                    fillColor: Colour.transparent,
                    borderColor: Colour.transparent,
                    borderThick: 0,
                    radius: 0,
                  ),
                  selectableDayPredicate: (date) {
                    if (widget.enableWorkingDays) {
                      // Disable weekend days to select from the calendar
                      if (widget.listWorkDays.contains(date.weekday)) {
                        return true;
                      }
                      // Return true if enable select
                      return false;
                    } else {
                      return true;
                    }
                  },
                  onChanged: widget.onChanged,
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
