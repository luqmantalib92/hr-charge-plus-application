import 'package:flutter/material.dart';

import '../style/style_color.dart';

import '../data/data_value.dart';

class ContainerBody extends StatelessWidget {
  final Widget child;
  ContainerBody({@required this.child});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: double.infinity,
      width: double.infinity,
      margin: EdgeInsets.only(top: Value.val100),
      padding: EdgeInsets.only(bottom: Value.val50),
      clipBehavior: Clip.hardEdge,
      decoration: BoxDecoration(
        color: Colour.bgScaffold,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(Value.val40),
          topRight: Radius.circular(Value.val40),
        ),
      ),
      child: child,
    );
  }
}
