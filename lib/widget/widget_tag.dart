import 'package:flutter/material.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';

import '../data/data_value.dart';

class Tag extends StatelessWidget {
  final String value;
  final String text;
  final Color bgColor;
  final Function onTap;
  Tag({
    @required this.value,
    @required this.text,
    @required this.bgColor,
    @required this.onTap,
  });
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: Container(
        margin: EdgeInsets.only(bottom: Value.val5),
        padding: EdgeInsets.symmetric(
          vertical: Value.val5,
          horizontal: Value.val10,
        ),
        decoration: BoxDecoration(
          color: bgColor,
          borderRadius: BorderRadius.all(
            Radius.circular(5),
          ),
        ),
        child: Row(
          mainAxisSize: MainAxisSize.min,
          children: [
            Text(
              text,
              style: StyleFont.inter12px700w(
                Colour.textPrimary,
              ),
            ),
            SizedBox(width: Value.val3),
            Container(
              padding: EdgeInsets.symmetric(horizontal: Value.val3),
              decoration: BoxDecoration(
                color: Colour.bgMedia,
                borderRadius: BorderRadius.all(
                  Radius.circular(5),
                ),
              ),
              child: Text(
                "x",
                style: StyleFont.inter12px400w(
                  Colour.textPrimary,
                ),
                textAlign: TextAlign.center,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
