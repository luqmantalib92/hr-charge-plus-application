import 'package:flutter/material.dart';
import 'package:dotted_border/dotted_border.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';

class AddChecklist extends StatelessWidget {
  final int totalChecklist;
  final Function onPressed;
  AddChecklist({
    @required this.totalChecklist,
    @required this.onPressed,
  });

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: Value.val8,
        horizontal: Value.val30,
      ),
      child: DottedBorder(
        color: Colour.iconFade,
        borderType: BorderType.RRect,
        radius: Radius.circular(10),
        strokeWidth: 2,
        child: Container(
          width: double.infinity,
          height: Value.val50,
          clipBehavior: Clip.hardEdge,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(10),
          ),
          child: FlatButton(
            onPressed: onPressed,
            child: Row(
              children: [
                Icon(
                  Icons.add,
                  size: Value.val20,
                  color: Colour.textSecondary,
                ),
                SizedBox(width: Value.val20),
                Text(
                  "Checklist " + (totalChecklist + 1).toString(),
                  style: StyleFont.inter16px500w(
                    Colour.textSecondary,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
