import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';

class CardSettings extends StatelessWidget {
  final String imagePath;
  final double imageSize;
  final Color imageColor;
  final String title;
  final String content;
  final Function onPressed;
  final bool enableImageColor;
  CardSettings({
    @required this.imagePath,
    @required this.imageSize,
    @required this.imageColor,
    @required this.title,
    @required this.content,
    @required this.onPressed,
    this.enableImageColor = true,
  });
  @override
  Widget build(BuildContext context) {
    return RaisedButton(
      onPressed: onPressed,
      color: Colour.white,
      elevation: 0,
      padding: EdgeInsets.symmetric(
        vertical: Value.val25,
        horizontal: Value.val15,
      ),
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(8)),
      child: Row(
        children: [
          Visibility(
            visible: !(imagePath == ""),
            child: Container(
              width: imageSize,
              child: enableImageColor
                  ? Image(
                      image: Image.asset(imagePath).image,
                      color: imageColor,
                    )
                  : Image(image: Image.asset(imagePath).image),
            ),
          ),
          SizedBox(width: Value.val10),
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  title,
                  style: StyleFont.inter13px400w(imageColor),
                ),
                Visibility(
                  visible: !(content == ""),
                  child: SizedBox(height: Value.val5),
                ),
                Visibility(
                  visible: !(content == ""),
                  child: Text(
                    content,
                    style: StyleFont.inter10px400w(imageColor),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
