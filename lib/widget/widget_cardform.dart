import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';

class CardForm extends StatefulWidget {
  final String id;
  final String title;
  final List data;
  CardForm({
    @required this.id,
    @required this.title,
    @required this.data,
  });

  @override
  _CardFormState createState() => _CardFormState();
}

class _CardFormState extends State<CardForm> {
  String _id = "0";
  String _title = "title";
  List<Widget> _checklist = [];

  // INIT STATE
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    buildChecklist();
  }

  // BUILD CHECKLIST
  void buildChecklist() {
    List<Widget> checklist = [];
    // Load data
    List data = widget.data;
    int dataLength = data.length;
    // Loop all data
    for (int i = 0; i < dataLength; i++) {
      checklist.add(
        Container(
          padding: EdgeInsets.symmetric(vertical: Value.val5),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Icon(
                Icons.check_box_outline_blank_rounded,
                color: Colour.textPrimary,
              ),
              SizedBox(width: Value.val10),
              Expanded(
                child: RichText(
                  text: TextSpan(
                    text: data[i]["task"],
                    style: StyleFont.inter14px400w(
                      Colour.textPrimary,
                    ),
                    children: <TextSpan>[
                      TextSpan(
                        text: "\nDurations: " + data[i]["duration"] + " Day(s)",
                        style: StyleFont.inter10px400w(
                          Colour.textSecondary,
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      );
      // Set global
      setState(() {
        _id = widget.id;
        _title = widget.title;
        _checklist = checklist;
      });
    }
  }

  // BUILD
  @override
  Widget build(BuildContext context) {
    return Container(
      width: double.infinity,
      margin: EdgeInsets.symmetric(
        horizontal: Value.val25,
        vertical: Value.val5,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Value.val20),
        color: Colour.bgPrimary,
      ),
      clipBehavior: Clip.hardEdge,
      child: Row(
        children: [
          Expanded(
            flex: 1,
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: Value.val10,
              ),
              child: Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  Text(
                    "$_id",
                    style: StyleFont.inter24px700w(Colors.white),
                  ),
                ],
              ),
            ),
          ),
          Expanded(
            flex: 4,
            child: Container(
              padding: EdgeInsets.all(Value.val10),
              decoration: BoxDecoration(
                color: Colour.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(Value.val20),
                  bottomRight: Radius.circular(Value.val20),
                ),
              ),
              child: Column(
                children: [
                  Text(
                    "$_title",
                    style: StyleFont.inter18px700w(
                      Colour.textTertiary,
                    ),
                  ),
                  SizedBox(height: Value.val5),
                  Container(
                    child: Column(
                      children: _checklist,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
