import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';

import '../screen_addstaff.dart';
import '../screen_hrstaff.dart';
import '../screen_addform.dart';
import '../screen_hrform.dart';
import '../screen_addrole.dart';
import '../screen_hrrole.dart';

enum pages { addNewStaff, addNewForm, addNewRole }

class ButtonFloating extends StatelessWidget {
  final String tooltip;
  final pages navigate;
  ButtonFloating({@required this.tooltip, @required this.navigate});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(bottom: Value.val70),
      child: FloatingActionButton(
        onPressed: () async {
          switch (navigate) {
            case pages.addNewForm:
              {
                await Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: ScreenAddForm(),
                  ),
                );
                // Reload whole page
                Navigator.pushReplacement(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: ScreenHRForm(),
                  ),
                );
              }
              break;
            case pages.addNewStaff:
              {
                await Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: ScreenAddStaff(),
                  ),
                );
                // Reload whole page
                Navigator.pushReplacement(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: ScreenHRStaff(),
                  ),
                );
              }
              break;
            case pages.addNewRole:
              {
                await Navigator.push(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: ScreenAddRole(),
                  ),
                );
                // Reload whole page
                Navigator.pushReplacement(
                  context,
                  PageTransition(
                    type: PageTransitionType.fade,
                    child: ScreenHRRole(),
                  ),
                );
              }
              break;
          }
        },
        child: Icon(Icons.add),
        backgroundColor: Colour.bgPrimary,
        tooltip: tooltip,
      ),
    );
  }
}
