import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';

class CardRole extends StatelessWidget {
  final String role;
  final String form;
  CardRole({
    @required this.role,
    @required this.form,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      height: Value.val80,
      width: double.infinity,
      margin: EdgeInsets.symmetric(
        horizontal: Value.val25,
        vertical: Value.val5,
      ),
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(Value.val5),
        color: Colour.white,
      ),
      clipBehavior: Clip.hardEdge,
      child: Row(
        children: [
          Container(
            width: Value.val10,
            height: double.infinity,
            color: Colour.bgPrimary,
          ),
          SizedBox(width: Value.val10),
          Expanded(
            child: Container(
              padding: EdgeInsets.all(Value.val10),
              decoration: BoxDecoration(
                color: Colour.white,
                borderRadius: BorderRadius.only(
                  topRight: Radius.circular(Value.val20),
                  bottomRight: Radius.circular(Value.val20),
                ),
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    "$role",
                    style: StyleFont.inter16px700w(
                      Colour.textTertiary,
                    ),
                  ),
                  SizedBox(height: Value.val5),
                  Row(
                    children: [
                      Icon(
                        Icons.attach_file_rounded,
                        size: Value.val15,
                        color: Colour.textSecondary,
                      ),
                      Text(
                        "$form",
                        style: StyleFont.inter12px400w(
                          Colour.textSecondary,
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            width: Value.val50,
            height: Value.val50,
            decoration: BoxDecoration(
              color: Colour.bgPrimary,
              borderRadius: BorderRadius.all(
                Radius.circular(Value.val100),
              ),
            ),
            clipBehavior: Clip.hardEdge,
            alignment: Alignment.center,
            child: Icon(
              Icons.person,
              color: Colour.white,
              size: Value.val25,
            ),
          ),
          SizedBox(width: Value.val10),
        ],
      ),
    );
  }
}
