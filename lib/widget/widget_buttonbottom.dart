import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';

import 'widget_buttonborder.dart';

class ButtonBottom extends StatelessWidget {
  final String text;
  final Function onPressed;
  ButtonBottom({
    @required this.text,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: Value.val30),
        height: Value.val130,
        width: double.infinity,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.end,
          children: [
            Row(
              children: [
                Expanded(
                  child: ButtonBorder(
                    text: "$text",
                    textColor: Colors.white,
                    backgroundColor: Colour.bgPrimary,
                    borderColor: Colour.bgPrimary,
                    onPressed: onPressed,
                    isIcon: false,
                  ),
                ),
              ],
            ),
            SizedBox(height: Value.val20),
          ],
        ),
      ),
    );
  }
}
