import 'package:flutter/material.dart';

import '../data/data_cache.dart';
import '../data/data_value.dart';

import '../style/style_font.dart';
import '../style/style_color.dart';
import '../style/style_decoration.dart';
import '../style/style_input.dart';

class WidgetDialog {
  // CHECKLIST DIALOG
  Future<dynamic> checklist({
    @required BuildContext context,
    @required int id,
    @required TextEditingController controllerTitle,
    @required TextEditingController controllerDuration,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => AlertDialog(
        elevation: 0,
        backgroundColor: Colour.bgScaffold,
        contentPadding: EdgeInsets.symmetric(
          horizontal: Value.val15,
          vertical: 0,
        ),
        content: Container(
          width: Value.val275,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: Value.val30),
              Container(
                alignment: Alignment.centerLeft,
                width: double.infinity,
                child: Text(
                  "Checklist Title",
                  style: StyleFont.inter14px600w(Colour.textSecondary),
                ),
              ),
              SizedBox(height: Value.val5),
              Container(
                decoration: StyleDecoration.basic(),
                child: TextFormField(
                  controller: controllerTitle,
                  style: StyleFont.inter16px400w(
                    Colour.textPrimary,
                  ),
                  keyboardType: TextInputType.text,
                  decoration: StyleInput().border(
                    hintText: '',
                    hintStyle: StyleFont.inter14px400w(
                      Colour.textSecondary,
                    ),
                    fillColor: Colour.white,
                    borderColor: Colour.transparent,
                    borderThick: 0,
                    radius: 10,
                  ),
                ),
              ),
              SizedBox(height: Value.val20),
              Container(
                alignment: Alignment.centerLeft,
                width: double.infinity,
                child: Text(
                  "Checklist Duration",
                  style: StyleFont.inter14px600w(Colour.textSecondary),
                ),
              ),
              SizedBox(height: Value.val5),
              Container(
                decoration: StyleDecoration.basic(),
                child: TextFormField(
                  controller: controllerDuration,
                  style: StyleFont.inter16px400w(
                    Colour.textPrimary,
                  ),
                  keyboardType: TextInputType.number,
                  decoration: StyleInput().border(
                    hintText: '',
                    hintStyle: StyleFont.inter14px400w(
                      Colour.textSecondary,
                    ),
                    fillColor: Colour.white,
                    borderColor: Colour.transparent,
                    borderThick: 0,
                    radius: 10,
                  ),
                ),
              ),
              SizedBox(height: Value.val40),
              Container(
                width: double.infinity,
                height: Value.val60,
                child: FlatButton(
                  color: Colour.bgScaffold,
                  child: Text(
                    "Update Checklist",
                    style: StyleFont.inter16px700w(Colour.bgPrimary),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(
                      width: 2,
                      color: Colour.bgPrimary,
                    ),
                  ),
                  onPressed: () {
                    Cache.checklist[id]["task"] = controllerTitle.text;
                    Cache.checklist[id]["duration"] = controllerDuration.text;
                    Navigator.pop(context);
                  },
                ),
              ),
              SizedBox(height: Value.val15),
            ],
          ),
        ),
      ),
    );
  }

  // ACKNOWLEGDE DIALOG
  Future<dynamic> acknowledged({
    @required BuildContext context,
    @required String title,
    @required String description,
    @required String titleButton,
    @required Function onPressedButton,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => AlertDialog(
        elevation: 0,
        contentPadding: EdgeInsets.symmetric(
          horizontal: Value.val15,
          vertical: 0,
        ),
        content: Container(
          width: Value.val275,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: Value.val30),
              Container(
                padding: EdgeInsets.symmetric(horizontal: Value.val15),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: StyleFont.inter24px600w(Colour.textTertiary),
                ),
              ),
              SizedBox(height: Value.val15),
              Container(
                padding: EdgeInsets.symmetric(horizontal: Value.val15),
                child: Text(
                  description,
                  textAlign: TextAlign.center,
                  style: StyleFont.inter18px400w(Colors.black),
                ),
              ),
              SizedBox(height: Value.val20),
              Container(
                width: double.infinity,
                height: Value.val60,
                child: FlatButton(
                  color: Colour.bgPrimary,
                  child: Text(
                    titleButton,
                    style: StyleFont.inter16px700w(Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(
                      width: 0,
                      color: Colour.bgPrimary,
                    ),
                  ),
                  onPressed: onPressedButton,
                ),
              ),
              SizedBox(height: Value.val15),
            ],
          ),
        ),
      ),
    );
  }

  // ERROR DIALOG
  Future<dynamic> error({
    @required BuildContext context,
    @required String title,
    @required String description,
    @required String titleButton,
  }) {
    return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) => AlertDialog(
        elevation: 0,
        contentPadding: EdgeInsets.symmetric(
          horizontal: Value.val15,
          vertical: 0,
        ),
        content: Container(
          width: Value.val275,
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              SizedBox(height: Value.val30),
              Container(
                padding: EdgeInsets.symmetric(horizontal: Value.val15),
                child: Text(
                  title,
                  textAlign: TextAlign.center,
                  style: StyleFont.inter24px600w(Colour.textTertiary),
                ),
              ),
              SizedBox(height: Value.val15),
              Container(
                padding: EdgeInsets.symmetric(horizontal: Value.val15),
                child: Text(
                  description,
                  textAlign: TextAlign.center,
                  style: StyleFont.inter18px400w(Colors.black),
                ),
              ),
              SizedBox(height: Value.val20),
              Container(
                width: double.infinity,
                height: Value.val60,
                child: FlatButton(
                  color: Colour.bgAlert,
                  child: Text(
                    titleButton,
                    style: StyleFont.inter16px700w(Colors.white),
                  ),
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(10),
                    side: BorderSide(
                      width: 0,
                      color: Colour.bgAlert,
                    ),
                  ),
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                ),
              ),
              SizedBox(height: Value.val15),
            ],
          ),
        ),
      ),
    );
  }
}
