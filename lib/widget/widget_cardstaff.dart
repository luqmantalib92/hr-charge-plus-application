import 'package:flutter/material.dart';

import '../data/data_value.dart';

import '../style/style_color.dart';
import '../style/style_font.dart';

class CardStaff extends StatelessWidget {
  final String id;
  final String name;
  final String profileURL;
  final Function onPressed;
  CardStaff({
    @required this.id,
    @required this.name,
    @required this.profileURL,
    @required this.onPressed,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(
        vertical: Value.val5,
        horizontal: Value.val25,
      ),
      decoration: BoxDecoration(
        color: Colour.white,
        borderRadius: BorderRadius.all(
          Radius.circular(
            Value.val10,
          ),
        ),
      ),
      child: FlatButton(
        height: Value.val80,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(
              Value.val10,
            ),
          ),
        ),
        color: Colour.white,
        padding: EdgeInsets.symmetric(
          horizontal: Value.val20,
        ),
        child: Row(
          children: [
            Container(
              width: Value.val50,
              height: Value.val50,
              decoration: BoxDecoration(
                color: Colour.bgPrimary,
                borderRadius: BorderRadius.all(
                  Radius.circular(Value.val100),
                ),
              ),
              clipBehavior: Clip.hardEdge,
              alignment: Alignment.center,
              child: Image.network(
                "$profileURL",
              ),
            ),
            SizedBox(width: Value.val15),
            Expanded(
              child: Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      "$name",
                      style: StyleFont.inter14px400w(
                        Colors.black,
                      ),
                    ),
                    SizedBox(height: Value.val5),
                    Text(
                      "$id",
                      overflow: TextOverflow.ellipsis,
                      style: StyleFont.inter13px400w(
                        Colour.bgSecondary,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
        onPressed: onPressed,
      ),
    );
  }
}
