# hrchargeplus

The goal is to create a mobile application using Flutter framework that will provide a list of
onboarding to-dos for managers to achieve a seamless onboarding experience for new
employees.
To provide a seamless onboarding experience, HR has the ability to establish a list of
onboarding to-dos and to assign it to managers. Assigned managers are able to view the list
and to mark each to-do as completed.

## Application Users

1. HR
- Username: cody
- Password: 1234
2. Manager
- Username: stone
- Password: 1234

## Application Files

[Download APK file to test the application.](https://drive.google.com/file/d/1iwTmo57xqj6q0MdnVTW7z1AvVrpvkp4P/view?usp=sharing)

## Application Screenshot
![Screenshot](screenshot/login.jpg)
![Screenshot](screenshot/staff.jpg)
![Screenshot](screenshot/role.jpg)
![Screenshot](screenshot/form.jpg)
![Screenshot](screenshot/checklist.jpg)

## Minimum Requirements

1. 2 views — HR view and manager view
2. Each of the checklist is for 1 new joiner
3. HR needs a form to create a standard checklist for all managers to view
4. Manager needs a checklist that is sorted with date and each task can be mark as
completed
5. Use any state management of your choice (preferably flutter_bloc)
Example :
18th January
[ ] Prepare laptop for John Doe
[ ] Prepare laptop bag for John Doe
19th January
[ ] Prepare access to Microsoft Office for John Doe

## Additional

1. Login for multiple users
2. HR can create different checklist for different staff role as default template
3. HR can assign created checklist to a specific or multiple manager
4. HR can view progress of the assigned checklist
5. User friendly design
6. Multiple structured Git commit messages
7. Tests

## Once you are done

1. Pushing commits to a private repository (GitHub/GitLab/Bitbucket) and grant access to
“developers@chargeplus.sg”
2. Build an APK installer for your app
3. Attached with screenshot of your app